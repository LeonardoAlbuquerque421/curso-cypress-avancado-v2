# Testes automatizados com Cypress - Inmetrics

👋 Seja bem-vinda(o)!

É muito bom tê-la(o) aqui. Tenho certeza que você vai adorar este projeto. ❤️

## O que você precisa saber.

- Cypress
- Testes de API
- Node

# Pré-requisitos

Antes de começar, garanta que os seguintes requisitos sejam atendidos:

- Computador com no mínimo 2 cores
- e no mínimo 8 GB de memória RAM

Além disso, garanta que os seguintes sistemas estejam instalados em seu computador:

- [git](https://git-scm.com/) (estou usando a versão `2.34.1`)
- [Node.js](https://nodejs.org/en/) (estou usando a versão `v16.13.2`)
- npm (estou usando a versão `8.19.2`)
- [Visual Studio Code](https://code.visualstudio.com/) (estou usando a versão `1.73.1`) ou alguma outra IDE de sua preferência

> **Obs.:** Recomendo utilizar as mesmas versões, ou versões mais recentes dos sistemas listados acima.
>
> **Obs. 2:** Ao instalar o Node.js o npm é instalado junto. 🎉
>
> **Obs. 3:** Para verificar as versões do git, Node.js e npm instaladas em seu computador, execute o comando `git --version && node --version && npm --version` no seu terminal de linha de comando.
>
> **Obs. 4:** Deixei links para os instaladores na lista de requisitos acima, caso não os tenha instalados ainda.


## Clonando o projeto 🐑

Abra o navegador, acesse a URL https://gitlab.com/LeonardoAlbuquerque421/curso-cypress-avancado-v2.git, clique no botão **Code**, escolha uma opção de clone (HTTPS ou SSH), copie o link de clone do projeto, e em seu terminal de linha de comando (em uma pasta onde você armazene seus projetos de software), execute o comando `git clone [cole-o-link-copiado-aqui]`.

Após o clone do projeto, acesse o diretório recém clonado (ex.: `cd curso-cypress-avancado-v2/`).

Dentro do diretório `curso-cypress-avancado-v2/` você terá os sub-diretórios `.git/` (diretório oculto), `cypress/` e `lessons/`, e os arquivos `.gitignore` (arquivo oculto), `cypress.json`, `LICENSE`, `package-lock.json`, `package.json` e `README.md`.

## Instalação das dependências de desenvolvimento

Com o projeto clonado a partir do **GitLab**, é hora de instalarmos suas depedências de desenvolvimento.

Visto que tais dependências já estão listadas no arquivo `package.json`, basta executar o comando `npm install` (ou `npm i` - versão curta) na raiz do projeto.

> 🧙 Este comando irá baixar o `cypress`, o `cypress-localstorage-commands`e as dependencias bibliotecas necessarias.

Execute o comando `npm test` (ou `npm t` - versão curta) para garantir que tudo está funcionando como deveria. Se tudo ocorrer conforme o esperado, você deve obter um resultado como o seguinte.![Alt text](image.png)
