const { defineConfig } = require("cypress");

module.exports = defineConfig({
  e2e: {
    experimentalMemoryManagement: true,
    numTestsKeptInMemory: 5,
    viewportWidth: 1920,
    viewportHeight: 1250,
    setupNodeEvents(on, _config) {
      on("task", {
        log(message) {
          console.log(message);
          return null;
        },
      });
    },
    supportFile: "cypress/support/e2e.js",
  },
  reporter: "junit",
  reporterOptions: {
    mochaFile: "cypress/results/output.xml",
    toConsole: true,
  },
  video: true,
  defaultCommandTimeout: 10000,
});
