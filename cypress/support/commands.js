import { faker } from "@faker-js/faker";

Cypress.Commands.add("GetListAllUsers", () => {
  return cy
    .request({
      method: "GET",
      url: `https://serverest.dev/usuarios`,
      failOnStatusCode: false,
    })
    .then((response) => {
      return response;
    });
});

Cypress.Commands.add("GetAUserFromTheList", (iduser) => {
  return cy
    .request({
      method: "GET",
      url: `https://serverest.dev/usuarios/${iduser}`,
      failOnStatusCode: false,
    })
    .then((response) => {
      return response;
    });
});

Cypress.Commands.add(
  "PostNewUser",
  (emailUser = faker.datatype.uuid() + "@qa.com.br") => {
    const requestCreate = {
      nome: faker.name.fullName(),
      email: emailUser,
      password: faker.datatype.uuid(),
      administrador: "true",
    };

    return cy
      .request({
        method: "POST",
        url: "https://serverest.dev/usuarios",
        body: requestCreate,
        failOnStatusCode: false,
      })
      .then((response) => {
        return response;
      });
  }
);

Cypress.Commands.add(
  "PutUser",
  (iduser, emailUser = faker.datatype.uuid() + "@qa.com.br") => {
    const requestCreate = {
      nome: faker.name.fullName(),
      email: emailUser,
      password: faker.datatype.uuid(),
      administrador: "true",
    };

    return cy
      .request({
        method: "PUT",
        url: `https://serverest.dev/usuarios/${iduser}`,
        body: requestCreate,
        failOnStatusCode: false,
      })
      .then((response) => {
        return response;
      });
  }
);

Cypress.Commands.add("DeleteUser", (iduser) => {
  return cy
    .request({
      method: "DELETE",
      url: `https://serverest.dev/usuarios/${iduser}`,
      failOnStatusCode: false,
    })
    .then((response) => {
      return response;
    });
});
