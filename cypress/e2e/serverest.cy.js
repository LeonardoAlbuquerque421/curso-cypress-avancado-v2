describe('ServeRest', () => {
    it('Returns a list of all users', () => {
      cy.GetListAllUsers().then((response) => {
        expect(200).to.eq(response.status);
        expect(response.body).to.have.property('quantidade');
        expect(response.body).to.have.property('usuarios').that.is.an('array');

        const primeiroUsuario = response.body.usuarios[0];      
        expect(primeiroUsuario).to.have.all.keys('nome', 'email', 'password', 'administrador', '_id');
        expect(primeiroUsuario.nome).to.be.a('string');
        expect(primeiroUsuario.email).to.be.a('string').and.match(/^[^\s@]+@[^\s@]+\.[^\s@]+$/); // Email válido
        expect(primeiroUsuario.password).to.be.a('string');
        expect(primeiroUsuario.administrador).to.be.oneOf(['true', 'false']); // Garantir que é string
        expect(primeiroUsuario._id).to.be.a('string');
      });  
    });

    it('Returns searched user', () => {
      cy.GetAUserFromTheList("0uxuPY0cbmQhpEz1").then((response) => {
        expect(200).to.eq(response.status);

        const responseBody = response.body;      
        
        expect(responseBody).to.have.all.keys('nome', 'email', 'password', 'administrador', '_id');
        expect(responseBody.nome).to.be.a('string');
        expect(responseBody.email).to.be.a('string').and.match(/^[^\s@]+@[^\s@]+\.[^\s@]+$/); // Email válido
        expect(responseBody.password).to.be.a('string');
        expect(responseBody.administrador).to.be.oneOf(['true', 'false']); // Garantir que é string
        expect(responseBody._id).to.be.a('string');
      });  
    });

    it('Validation error message User Not Found', () => {
        cy.GetAUserFromTheList("1").then((responseGetUser) => {
          expect(400).to.eq(responseGetUser.status);
          expect(responseGetUser.body).to.have.property('message', 'Usuário não encontrado');
      });  
    });
    
    it('Post new user', () => {
      // NEW USER POSTED IN THE BANK.
      cy.PostNewUser().then((response) => {
        expect(201).to.eq(response.status);
        expect(response.body).to.have.all.keys('message', '_id');
        expect(response.body).to.have.property('message', 'Cadastro realizado com sucesso');
        expect(response.body).to.have.property('_id').that.is.a('string');

        // GET USER PREVIOUSLY CREATED
        cy.GetAUserFromTheList(response.body._id).then((responseGetUser) => {
          expect(200).to.eq(responseGetUser.status);
  
          const responseBody = responseGetUser.body;      
          
          expect(responseBody).to.have.all.keys('nome', 'email', 'password', 'administrador', '_id');
          expect(responseBody.nome).to.be.a('string');
          expect(responseBody.email).to.be.a('string').and.match(/^[^\s@]+@[^\s@]+\.[^\s@]+$/);
          expect(responseBody.password).to.be.a('string');
          expect(responseBody.administrador).to.be.oneOf(['true', 'false']);
          expect(responseBody._id.toLowerCase()).to.eq(response.body._id.toLowerCase());
        });  
      });  
    });
    
    it('Validation error message Email must be a valid email', () => {
      cy.PostNewUser("1").then((response) => {
        expect(400).to.eq(response.status);
        expect(response.body).to.have.property('email', 'email deve ser um email válido');
    });
    });
    
    it('Validation error message This email is already in use', () => {
      // NEW USER POSTED IN THE BANK.
      cy.PostNewUser().then((responsePost) => {
        expect(201).to.eq(responsePost.status);
        expect(responsePost.body).to.have.all.keys('message', '_id');
        expect(responsePost.body).to.have.property('message', 'Cadastro realizado com sucesso');
        expect(responsePost.body).to.have.property('_id').that.is.a('string');

        // GET USER PREVIOUSLY CREATED
        cy.GetAUserFromTheList(responsePost.body._id).then((responseGetUser) => {
          expect(200).to.eq(responseGetUser.status);

            cy.PostNewUser(responseGetUser.body.email).then((response) => {
              expect(400).to.eq(response.status);
              expect(response.body).to.have.keys('message');
              expect(response.body).to.have.property('message', 'Este email já está sendo usado');
          });  
        });
      });  
    });
    
    it('Update user it the bank', () => {
      // NEW USER POSTED IN THE BANK.
      cy.PostNewUser().then((responsePost) => {
        expect(201).to.eq(responsePost.status);
        expect(responsePost.body).to.have.all.keys('message', '_id');
        expect(responsePost.body).to.have.property('message', 'Cadastro realizado com sucesso');
        expect(responsePost.body).to.have.property('_id').that.is.a('string');

        // GET USER PREVIOUSLY CREATED
        cy.GetAUserFromTheList(responsePost.body._id).then((responseGetUser) => {
          expect(200).to.eq(responseGetUser.status);

          // UPDATE USER PREVIOUSLY CREATED
          cy.PutUser(responsePost.body._id).then((responsePut) => {
            expect(200).to.eq(responsePut.status);
            expect(responsePut.body).to.have.keys('message');
            expect(responsePut.body).to.have.property('message', 'Registro alterado com sucesso');
        });  
      });  
    });
    });
  
    it('Delete user it the bank', () => {
      // NEW USER POSTED IN THE BANK.
      cy.PostNewUser().then((responsePost) => {
        expect(201).to.eq(responsePost.status);
        expect(responsePost.body).to.have.all.keys('message', '_id');
        expect(responsePost.body).to.have.property('message', 'Cadastro realizado com sucesso');
        expect(responsePost.body).to.have.property('_id').that.is.a('string');

        // GET USER PREVIOUSLY CREATED
        cy.GetAUserFromTheList(responsePost.body._id).then((responseGetUser) => {
          expect(200).to.eq(responseGetUser.status);

          // DELETE USER PREVIOUSLY CREATED
          cy.DeleteUser(responsePost.body._id).then((responseDelete) => {
            expect(200).to.eq(responseDelete.status);
            expect(responseDelete.body).to.have.keys('message');
            expect(responseDelete.body).to.have.property('message', 'Registro excluído com sucesso');
        });  
      });  
    });
    });
  
    it('Validation error message No records deleted', () => {
          cy.DeleteUser("1").then((responseDelete) => {
            expect(200).to.eq(responseDelete.status);
            expect(responseDelete.body).to.have.keys('message');
            expect(responseDelete.body).to.have.property('message', 'Nenhum registro excluído');
        });  
    });
})